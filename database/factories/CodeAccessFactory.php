<?php

namespace Database\Factories;

use App\Models\CodeAccess;
use Illuminate\Database\Eloquent\Factories\Factory;

class CodeAccessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CodeAccess::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
