<?php

namespace Database\Factories;

use App\Models\TypeAchat;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeAchatFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TypeAchat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
