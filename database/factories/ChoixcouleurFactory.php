<?php

namespace Database\Factories;

use App\Models\Choixcouleur;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChoixcouleurFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Choixcouleur::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
