<?php

namespace Database\Factories;

use App\Models\Type_evenement;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeEvenementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Type_evenement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
