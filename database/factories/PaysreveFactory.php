<?php

namespace Database\Factories;

use App\Models\Paysreve;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaysreveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paysreve::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
