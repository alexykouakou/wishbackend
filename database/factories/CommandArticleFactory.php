<?php

namespace Database\Factories;

use App\Models\CommandArticle;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommandArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CommandArticle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
