<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            // $table->string('photo')->nullable();
            $table->string('nom')->nullable();
            $table->string('date_naissance')->nullable();
            $table->string('phone')->nullable();
            $table->string('civilite')->nullable();
            $table->string('prenom')->nullable();
            $table->string('photo')->nullable();
            // $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('choixcouleur_id')->nullable();
            $table->unsignedBigInteger('paysreve_id')->nullable();
            $table->string('capitale_pays')->nullable();
            $table->string('population_pays')->nullable();
            $table->string('api_token')->nullable();
            // $table->string('photo')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
