<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequecadeausTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chequecadeaus', function (Blueprint $table) {
            $table->id();
            $table->string('montant_cadeau')->nullable();
            $table->string('commentaire')->nullable();
            $table->string('user_id')->nullable();
            $table->string('code_bar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chequecadeaus');
    }
}
