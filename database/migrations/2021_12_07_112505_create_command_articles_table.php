<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('command_articles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("article_id")->nullable();
            $table->unsignedBigInteger("commande_id")->nullable();
            $table->integer("quantite")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('command_articles');
    }
}
