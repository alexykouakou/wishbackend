<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recuperation de compte</title>
</head>
<body>
    <h1>Code de recuperation</h1>
    <div style="margin-top: 10px;">
        Voici votre code de recuperation de votre compte: <br>
        <span style="font-size: 1.3em; font-weight: bold; font-style: italic">{{$code_access}}</span>
    </div>
</body>
</html>