<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageDestinataire extends Model
{
    use HasFactory;

    public function destinataires(){
        return $this->hasMany(User::class, 'id_destinataire');
    }

    public function message(){
        return $this->belongsTo(Message::class, 'id_message');
    }
}
