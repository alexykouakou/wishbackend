<?php

namespace App\Models;

use App\Models\CodeAccess;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $guarded = [];

    public function access_codes()
    {
        return $this->hasMany(CodeAccess::class, 'fk_user');
    }


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function subscriptions()
    {
        return $this->belongsToMany(User::class, 'abonnements', 'id_utilisateur_suivi', 'id_utilisateur_abonne');
    }

    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'abonnements', 'id_utilisateur_abonne', 'id_utilisateur_suivi');
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
    
    public function getPhotoAttribute($photo){
        if(!$photo) {
            return "https://plchldr.co/i/100x100?text=no-photo&bg=023485";
        }
        return $photo;
    }
    
    
    public function phones()
    {
        return $this->morphMany(Phone::class, 'phoneable');
    }

    public function getIsPhoneVerifiedAttribute() {
        return $this->phones()->where('verified', true)->first() ? true : false;
    }

    public function getPhoneAttribute() {
        return $this->phones()->where('default', true)->latest()->first()->phone;
    }

    public function getPhoneNameAttribute() {
        return $this->phones()->where('default', true)->latest()->first()->name;
    }
}
