<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Config;

class Image extends Model
{
    use HasFactory;
    
    protected $fillable = ([
        'path', 'height', 'width'
    ]);
    
    public function evenement() {
        return $this->belongsTo(Evenement::class);
    }
    
    public function getPathAttribute($path) {
        return Config::get('app.url').'/'.$path;
    }
}
