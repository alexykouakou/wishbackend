<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Config;

class Commentaire extends Model
{
    use HasFactory;

     protected $guarded= [];
     
     protected $with = ['user'];

     public function likes(){
        return $this->morphMany(Like::class, 'likeable');
    }
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function getAudioAttribute($audio) {
        if($audio) {
            return Config::get('app.url').'/storage/'.$audio;
        }
        return;
    }
}
