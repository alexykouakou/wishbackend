<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    public function expediteur(){
        return $this->belongsTo(User::class, 'id_expediteur');
    }

    public function reponse(){
        return $this->belongsTo(Message::class, 'id_parent');
    }
}
