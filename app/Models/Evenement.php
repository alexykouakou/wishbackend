<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
    use HasFactory;
    
    protected $fillable = ['titre', 'user_id', 'localisation', 'description', 'type'];

    public function likes(){
        return $this->morphMany(Like::class, 'likeable');
    }
    
    public function images() {
        return $this->hasMany(Image::class);
    }
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function commentaires() {
        return $this->hasMany(Commentaire::class);
    }
}
