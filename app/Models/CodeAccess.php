<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CodeAccess extends Model
{
    use HasFactory;
    protected $table = 'access_code';

    public function user()
    {
        $this->belongsTo(User::class, 'fk_user');
    }
}
