<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PhoneToken extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'token', 'used', 'used_at'
    ];

    public function phone() {
        return $this->belongsTo(Phone::class);
    }

    public function getIsValidAttribute() {
        $minutes = Carbon::now()->diffInMinutes(Carbon::parse($this->created_at));
        if($minutes > 15) {
            return false;
        }
        return true;
    }
}
