<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'phone', 'name', 'verified', 'default', 'blacklisted'
    ];

    public function phoneable()
    {
        return $this->morphTo();
    }

    public function tokens() {
        return $this->hasMany(PhoneToken::class);
    }
}
