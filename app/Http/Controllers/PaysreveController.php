<?php

namespace App\Http\Controllers;

use App\Models\Paysreve;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;

class PaysreveController extends Controller
{

    /**
     * @OA\Post(
     *      path="/api/ajouterpaysReserve",
     *      operationId="ajouterpaysReserve",
     *      tags={"pays de reve"},
     *      summary="Permet ajoute les pays",
     *      description="Retourner les pays",
     *@OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"nom_pays"},
     *       @OA\Property(property="nom_pays", type="string", example="Cote d'ivoire"),
     *     
     *     
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur  veillez  ressaisir",
     *      ),
     *      
     *     )
     */



    /**
     * @OA\Get(
     *      path="/api/getpaysReserve",
     *      operationId="getpaysReserve",
     *      tags={"pays de reve"},
     *      summary="Permet affiche les  pays",
     *      description="Retourner la liste pays",
     * 
   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur  veillez  ressaisir",
     *      ),
     *      
     *     )
     */

    public function index()
    {

        $paysreserve = Paysreve::all();
        return Response()->json(["success" => $paysreserve]);
    }


    public function create()
    {
        //
    }


    public function ajouterpaysReserve(Request $request)
    {
        $request->validate([
            "nom_pays" => "required",
        ]);
        $paysreserve = Paysreve::create($request->all());
        return Response()->json(["success" => $paysreserve]);
    }


    public function show(Paysreve $paysreve)
    {
        //
    }


    public function edit(Paysreve $paysreve)
    {
        //
    }


    public function update(Request $request, Paysreve $paysreve)
    {
        //
    }


    public function destroy(Paysreve $paysreve)
    {
        //
    }
}
