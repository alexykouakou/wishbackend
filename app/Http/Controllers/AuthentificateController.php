<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Authentificate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\StoreAuthentificateRequest;
use App\Http\Requests\UpdateAuthentificateRequest;

class AuthentificateController extends Controller
{


    public function register(Request $request)
    {

        $fields = $request->validate([

            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:6',
            // 'phone' => 'required|string|confirmed|min:8',
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
        ]);

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            "user" => $user,
            "token" => $token
        ];
        return response()->json($response ,201);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAuthentificateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthentificateRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Authentificate  $authentificate
     * @return \Illuminate\Http\Response
     */
    public function show(Authentificate $authentificate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Authentificate  $authentificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Authentificate $authentificate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAuthentificateRequest  $request
     * @param  \App\Models\Authentificate  $authentificate
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuthentificateRequest $request, Authentificate $authentificate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Authentificate  $authentificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authentificate $authentificate)
    {
        //
    }
}
