<?php

namespace App\Http\Controllers;

use App\Models\CodeAccess;
use Illuminate\Http\Request;

class CodeAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CodeAccess  $codeAccess
     * @return \Illuminate\Http\Response
     */
    public function show(CodeAccess $codeAccess)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CodeAccess  $codeAccess
     * @return \Illuminate\Http\Response
     */
    public function edit(CodeAccess $codeAccess)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CodeAccess  $codeAccess
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CodeAccess $codeAccess)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CodeAccess  $codeAccess
     * @return \Illuminate\Http\Response
     */
    public function destroy(CodeAccess $codeAccess)
    {
        //
    }
}
