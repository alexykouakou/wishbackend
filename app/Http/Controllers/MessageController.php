<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\MessageDestinataire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function sendMessage(){
        
        $message = new Message();

        $message->id_expediteur = Auth::user()->id;
        $message->id_type_message = request()->id_type_message;
        $message->contenu = request()->contenu;

        $message->save();

        $getLastMessageId = Message::latest()->id;

        foreach(request()->id_destinataires as $destinataire){
            $messageDestinaire = new MessageDestinataire();

            $messageDestinaire->id_message = $getLastMessageId;
            $messageDestinaire->id_destinataire = $destinataire;

            $messageDestinaire->save();
        }

        return response()->json(['message' => $message]);
    }
}
