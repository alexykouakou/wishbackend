<?php

namespace App\Http\Controllers;

use App\Models\Abonnement;
use App\Models\User;
use App\Mail\Mailwishmee;
use App\Models\CodeAccess;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Facade\FlareClient\Http\response;
use Illuminate\Auth\Notifications\ResetPassword;
use App\Models\Phone;
use App\Models\PhoneToken;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:sanctum', ['except' => ['postlogin', 'postinscription', 'resetPassword', 'set_new_password']]);
    }

    /**
     * @OA\Post(
     *      path="/api/postlogin",
     *      operationId="getProjectsList",
     *      tags={"Connection"},
     *      summary="Permet à l'utilisateur de se connecter",
     *      description="Retourner les données de l'utilisateur",
     *@OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *       @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur de connection veillez créer un compte",
     *      ),
     *      
     *     )
     */

    /**
     * @OA\Post(
     *      path="/api/postinscription",
     *      operationId="getProjectsList",
     *      tags={"Inscription"},
     *      summary="Permet à l'utilisateur de s'inscrire",
     *      description="Retourner les données de l'utilisateur",
     *@OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password","nom","prenom","phone","civilite","date_Naissance"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *       @OA\Property(property="nom", type="string",  example="Kouakou"),
     *       @OA\Property(property="prenom", type="string",  example="Francis"),
     *       @OA\Property(property="phone", type="string"),
     *       @OA\Property(property="civilite", type="string"),
     *       @OA\Property(property="date_Naissance", type="string"),
     *       @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *     
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur  veillez  reprendre",
     *      ),
     *      
     *     )
     */

    /**
     * @OA\Post(
     *      path="/api/createProfile",
     *      operationId="createProfile",
     *      tags={"Creation du profile"},
     *      summary="Permet à l'utilisateur de creer un profile",
     *      description="Envoyer les données en base pour la création de profile",
     *@OA\RequestBody(
     *    required=true,
     *    description="Creation de profil",
     *    @OA\JsonContent(
     *       required={"choixcouleur_id","paysreve_id", "photo","capitale_pays","population_pays"},
     *       @OA\Property(property="choixcouleur_id", type="integer",  example="1"),
     *       @OA\Property(property="paysreve_id", type="integer",  example="1"),
     *       @OA\Property(property="photo", type="file", example="photo.jpg"),
     *       @OA\Property(property="capitale_pays", type="string", example="Abidjan"),
     *       @OA\Property(property="population_pays",type="string", example="22 millions "),
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur",
     *      ),
     *      
     *     )
     */


    /**
     * @OA\Get(
     *      path="/api/logout",
     *      operationId="logout",
     *      tags={"Deconnexion de l'utilisateur"},
     *      summary="Déconnexion de l'utilisateur",
     *      description="Deconnection",
     * 
   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur",
     *      ),
     *      
     *     )
     */


    /**
     * @OA\Post(
     *      path="/api/reset-password",
     *      operationId="reset-password",
     *      tags={"Reintiliser mot de passe"},
     *      summary="Mot de passe oublie",
     *      description="Genere un autre mot de passe",
     *@OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur  veillez vérifier votre email",
     *      ),
     *      
     *     )
     */


    /**
     * @OA\Post(
     *      path="/api/new-password",
     *      operationId="new-password",
     *      tags={"Nouveau  mot de passe"},
     *      summary="Nouveau  mot de passe",
     *      description="Genere un autre mot de passe",
     *@OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"password"},
     *       @OA\Property(property="password", type="string", format="password"),
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur  veillez saisir un nouveau mot de passe",
     *      ),
     *      
     *     )
     */
    public function postlogin(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'password' => 'required'

        ]);

        //PASSPORT

        $credentials = request(['email', 'password']);
        $user = User::where('email', $request->email)->first();

        if (!$api_token = auth()->attempt($credentials)) {


            if ($user) {
                return response()->json(['error' => 'password_error'], 401);
            }
            return response()->json(['error' => 'user_not_found'], 401);
        }

        $api_token = $user->createToken($user->api_token)->plainTextToken;

        $response = [
            "user" => $user,
            "access_token" => $api_token
        ];
        return response()->json($response);
    }



    //profile complete

    public function profileuser(Request $request)
    {
        // $user = Auth::user()->id;
        //connexion au compte
        $user = $request->validate([
            'choixcouleur_id' => 'required',
            'paysreve_id' => 'required',
            'capitale_pays' => 'required',
        ]);
        
        $user = auth()->user();
        
        $user->update([
            'choixcouleur_id' => $request->choixcouleur_id,
            'paysreve_id' => $request->paysreve_id,
            'capitale_pays' => $request->capitale_pays,
            'population_pays' => $request->population_pays,

        ]);
        
        if($request->file('photo')) {
            $path = $photo->store('public'.'/photo/user-'.$user->id);
            $real_path = Str::replaceFirst('public', 'storage', $path);
            
            $img = Image::make($real_path)->widen(736, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            
            $user->photo = real_path;
            $user->save();
        }
       return response()->json($user); 
        

    }




    public function create()
    {
        //
    }


    public function postinscription(Request $request)
    {
        //dd($request);
        $request->validate([
            'email' => 'unique:users|email',
            'password' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
            'phone' => 'required',
            'civilite' => 'required',
            'date_Naissance' => 'required',
        ]);

        $initialEmail = $request->email;
        $initialPassword = $request->password;
        $initial1 = $initialEmail[0];
        $initial2 = $initialPassword[0];
        $user = new User();
        $user->email = $request->email;
        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->phone = $request->phone;
        $user->civilite = $request->civilite;
        $user->date_Naissance = $request->date_Naissance;
        $user->password  = bcrypt($request->password);
        $token = ($initial1 . $initial2 . rand(1, 9999) . '-' . "WHISMEE-PROJET");
        $token = Str::random(80);
        $user->api_token = $token;
        $user->save();
        return response()->json(['success' => $user]);
    }
    
    public function postinscriptionphone(Request $request)
    {
        
        $number = str_replace([' ', '+'], '', $request->country_code.$request->phone);
        $numeric = is_numeric($number);
        if($numeric) {
            $check_is_number_exist = Phone::where('phone', $number)->first();
            if($check_is_number_exist && $check_is_number_exist->blacklisted) {
                return response()->json('account_deactivate', 422);
            }
    
         if($check_is_number_exist) {
                return response()->json('account_exist', 422);
            } else {
                $request->merge([
                    'email' => $number."@whismee.app",
                ]);
            } 
        }
        
        //dd($request);
        $request->validate([
            'country_code' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
            'phone' => 'required',
            'civilite' => 'required',
            'date_Naissance' => 'required',
        ]);
        
        

        $initialEmail = $request->email;
        $initialPassword = $request->password;
        $initial1 = $initialEmail[0];
        $initial2 = $initialPassword[0];
        $user = new User();
        $user->email = $request->email;
        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->phone = $request->phone;
        $user->civilite = $request->civilite;
        $user->date_Naissance = $request->date_Naissance;
        $user->password  = bcrypt($request->password);
        $token = ($initial1 . $initial2 . rand(1, 9999) . '-' . "WHISMEE-PROJET");
        $token = Str::random(80);
        $user->api_token = $token;
        $user->save();
        
        
        $phone = $user->phones()->create([
            'phone' => $number,
            'verified' => false,
            'default' => true,
            'name' => ''          
        ]);
        
        \App\Http\Traits\OTP::send($phone);
        
        return response()->json(['success' => $user]);
    }
    
    public function postinscriptionphonevalidate(Request $request) {
        $request->validate([
           'token' => 'required|numeric|digits:4',
       ]);
       
       $user = User::where('id', $request->id)->first();

       $phone =  $user()->phones()->where('verified', false)->first();
       if(!$phone) {
            return redirect()->back()->with('error', "Une erreur est survenue! ce utilisateur n'a pas téléphone non vérifié");
        }
       $exist_token = $phone->tokens()->latest()->first();

       if($exist_token->token == $request->token) {
           //set token as used and add used_at
           $exist_token->update([
               'used' => true,
               'used_at' => Carbon::now()
           ]);
           
           //set number as verified and default
           $phone->update([
               'verified' => true,
               'default' => true,
           ]);
           
           //return to app/index
           return response()->json('success', 200);
       }
       return response()->json("invalid_code", 422);
    }


    public function logout(Request $request)
    {
        $user =  Auth::logout();
        return response()->json($user, '200');
    }
    
    
    public function resetPasswordPhone(Request $request)
    {

        $number = str_replace([' ', '+'], '', $request->country_code.$request->phone);
        $numeric = is_numeric($number);
        
        $form = $request->validate([
            'phone' => ['required', 'string'],
            'country_code' => ['required', 'string'],
        ]);
        
        if($numeric) {
            $check_is_number_exist = Phone::where('phone', $number)->first();
            if($check_is_number_exist && $check_is_number_exist->blacklisted) {
                return response()->json('account_deactivate', 422);
            }
    
         if($check_is_number_exist) {
                //send code
                \App\Http\Traits\OTP::send($phone);
                return response()->json('success');
            } 
        }
        
        return response('phone_not_found', 422);
    }



    public function resetPassword(Request $request)
    {

        $form = $request->validate([
            'email' => ['required', 'string', 'exists:users,email']
        ]);
        $email = $form['email'];
        $user_to_reset_password = User::where('email', $email)->first();
        // on supprime les anciens code genere
        foreach ($user_to_reset_password->accesses_codes as $access) {
            CodeAccess::find($access->id)->delete();
        }

        $digits = '0123456789';
        while (true) {
            /* on melange les caracteres */
            $digits_mixed = str_shuffle($digits);

            //max length is fixed to six digits
            $max_length = 6;
            $final_code = '';
            for ($i = 0; $i < $max_length; $i++) {
                $index = rand(0, $max_length);
                $final_code .= $digits_mixed[$index];
            }
            $verify_uniqueness_code_access = CodeAccess::where('code', '=', $final_code)->get();
            if (!$verify_uniqueness_code_access->count()) {
                // le code genere est unique
                Mail::to($email)->send(new Mailwishmee($final_code));
                DB::table('access_code')->insert([
                    'code' => $final_code,
                    'fk_user' => $user_to_reset_password->id
                ]);
                return response()->json(['user_id' => $user_to_reset_password->id]);
            }
        }
        return response($user_to_reset_password, 200);
    }

    // public function new_password(Request $request, $id_user)
    // {
    //     return response()->json(['id_user' => $id_user]);
    // }

    public function set_new_password(Request $request)
    {
        $form = $request->validate([
            'code' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6', 'max:15'],
            'user_id' => ['required', 'integer']
        ]);
        $user_to_reset_password = User::find($form['user_id']);
        if ($user_to_reset_password == null) {
            return response()->json('error', "veuillez reprendre");
        }
        $code = $user_to_reset_password->accesses_codes;
        if (sizeof($code) == 0 || $code[0]->code != $form['code']) {
            return response()->json('info', 'Le code n\'est pas valide');
        }
        $user_to_reset_password->password = Hash::make($form['password']);
        $user_to_reset_password->save();
        CodeAccess::find($code[0]->id)->delete();

        return response()->json($user_to_reset_password);
    }
    
    public function set_new_password_phone(Request $request)
    {
        $request->validate([
            'country_code' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6', 'max:15'],
            'user_id' => ['required', 'integer']
        ]);
        
        $number = str_replace([' ', '+'], '', $request->country_code.$request->phone);
        $numeric = is_numeric($number);

        if($numeric) {
            $check_is_number_exist = Phone::where('phone', $number)->first();
            if($check_is_number_exist && $check_is_number_exist->blacklisted) {
                return response()->json('account_deactivate', 422);
            }
        $user = User::where('id', $request->user_id)->first();
         if($check_is_number_exist) {
                //new password
                $user->password  = bcrypt($request->password);
                $user->save();
                return response()->json('success');
            } 
        }
        
        return response('phone_not_found', 422);
    }



    public function getUsersToMention()
    {
        $abonnes = User::find(Auth::user()->id)->subscribers();

        return response()->json(['abonnes' => $abonnes]);
    }



    public function getSuggestion()
    {
        $users = User::all('id', 'nom', 'prenom', 'photo')->random(20);

        return response()->json(['users' => $users]);
    }


    public function subscribe()
    {
        $id_utilisateur = Auth::user()->id;
        $id_abonnement = request()->id_abonnement;

        $abonnement = new Abonnement();

        $abonnement->id_utilisateur_abonne = $id_utilisateur;
        $abonnement->id_utilisateur_suivi = $id_abonnement;

        $abonnement->save();

        return response()->json(['success', true]);
    }


    public function unSubscribe()
    {
        $id_utilisateur = Auth::user()->id;
        $id_abonnement = request()->id_abonnement;

        $abonnement = Abonnement::where([
            ['id_utilisateur_abonne', $id_utilisateur],
            ['id_utilisateur_suivi', $id_abonnement]
        ])->first();

        $abonnement->status = false;

        $abonnement->save();

        return response()->json(['success', true]);
    }

    public function show($id)
    {
        //
    }




    public function edit($id)
    {
        //
    }


    public function update(Request $request, $user)
    {
        $request->validate([
            'email' => 'unique:users,email',
            'password' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'civilite' => 'required',
            'date_Naissance' => 'required',
        ]);

        $user->update($request->all());

        return response()->json($user, '200');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return response()->json($user, '200');
    }
}
