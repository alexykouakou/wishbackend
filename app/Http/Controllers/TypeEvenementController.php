<?php

namespace App\Http\Controllers;

use App\Models\Type_evenement;
use Illuminate\Http\Request;

class TypeEvenementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type_evenement  $type_evenement
     * @return \Illuminate\Http\Response
     */
    public function show(Type_evenement $type_evenement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type_evenement  $type_evenement
     * @return \Illuminate\Http\Response
     */
    public function edit(Type_evenement $type_evenement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type_evenement  $type_evenement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type_evenement $type_evenement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type_evenement  $type_evenement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type_evenement $type_evenement)
    {
        //
    }
}
