<?php

namespace App\Http\Controllers;

use App\Models\Panier;
use App\Models\Article;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PanierController extends Controller
{

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $user  =  Auth::user();
        $data_validate = $request->validate([
            "quantite" => "integer|min:1",
            "article_id" => "required",
            "user_id" => "required",

        ]);

        // $article =  Article::first();
        $verify_article = Article::find($data_validate['article_id']);
        if ($verify_article) {
            if ($user != null) {

                $panier = Panier::create([

                    "user_id" => $user,
                    "article_id" => $verify_article,
                    "quantite" => $request->quantite,
                ]);
                return Response()->json(["success" => $panier]);
            }
        } else {

            return Response()->json(["error", "echec"]);
        }
    }


    public function show(Panier $panier)
    {
        //
    }


    public function edit(Panier $panier)
    {
        //
    }


    public function update(Request $request, Panier $panier)
    {
        //
    }


    public function destroy(Panier $panier)
    {
        //
    }
}
