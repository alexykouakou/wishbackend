<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Hashtag;
use App\Models\Evenement;
use App\Models\HashtagPost;
use App\Models\Like;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Type_evenement;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\EvenementResource;

use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class EvenementController extends Controller
{
   
    
    /**
     * @OA\Post(
     *      path="/api/postevent",
     *      operationId="getProjectsList",
     *      tags={"Poster un evenement"},
     *      summary="Poster un evenement",
     *      description="Enregistrer les evenements  ",
     *@OA\RequestBody(
     *    required=true,
     *    description="Enregistrer les evenements",
     *    @OA\JsonContent(
     *       required={"titre","user_id","description","localisation","photo_evenement"},
     *       @OA\Property(property="titre", type="string",example="rencontre des jeunes"),
     *       @OA\Property(property="user_id", type="integer",example="1"),
     *       @OA\Property(property="description", type="string", example="Un evenement organisé pour l'emploi"),
     *       @OA\Property(property="localisation", type="string",example="sofitel ivoire"),
     *       @OA\Property(property="photo_evenement", type="string",example="event.jpg"),
     *    ),
     * ),
   
     *   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur",
     *      ),
     *      
     *     )
     */
    public function getevents()

    {
        $posts = Evenement::latest()->get();
        $posts = EvenementResource::collection($posts);
        $response = [
            'posts' => $posts
        ];
        return response()->json($response);
    }

    public function getRandomEvents()
    {
        $events = Evenement::all('id', 'photo_evenement')->random(50);

        return response()->json(['evenements' => $events]);
    }

    public function searchEvent()
    {
        $search = request()->search_term;
        $events = Evenement::where('title', 'like', $search)
            ->orWhere('description', 'like', $search)
            ->orWhere('localisation', 'like', $search)
            ->get();

        return response()->json(['evenements' => $events]);
    }

    public function create()
    {
        //
    }

    public function posteventType(Request $request)
    {

        $request->validate([
            'nom' => 'required'
        ]);

        $eventType = Type_evenement::create($request->all());

        return Response()->json(['success' => $eventType]);
    }


    public function postevent(Request $request)
    {
       
        $user = auth()->user();
        
        $event = Evenement::create([
             'titre' => $request->titre,
             'user_id' => $user->id,
             'description' => $request->description,
             'type' => 'wish',
            //  'localisation' => '',
            //  'photo_evenement' => '',
        ]);
        
        //upload photos
        $medias = [];
        
        if($request->file('photo_evenement')) {
            foreach($request->file('photo_evenement') as $photo) {
                
                $path = $photo->store('public'.'/evenements/user-'.$user->id.'/evenemennt-'.$event->id);
                $real_path = Str::replaceFirst('public', 'storage', $path);
                
                $img = Image::make($real_path)->widen(736, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save();
    
                $medias[] = $event->images()->create([
                    'path' => $real_path,
                    'height' => Image::make($photo)->height(),
                    'width' => Image::make($photo)->width()
                ]);
    
            }
        }

        return response()->json('success');

    }
    
    public function posteventevent(Request $request)
    {
       
        $user = auth()->user();
        
        $event = Evenement::create([
             'titre' => $request->titre,
             'user_id' => $user->id,
             'description' => $request->description,
             'type' => 'event',
             'localisation' => '',
             'begin_date' => $request->begin_date,
             'end_date' => $request->end_date,
        ]);
        
        //upload photos
        $medias = [];
        
        if($request->file('photo_evenement')) {
            foreach($request->file('photo_evenement') as $photo) {
                
                $path = $photo->store('public'.'/evenements/user-'.$user->id.'/evenemennt-'.$event->id);
                $real_path = Str::replaceFirst('public', 'storage', $path);
                
                $img = Image::make($real_path)->widen(736, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save();
    
                $medias[] = $event->images()->create([
                    'path' => $real_path,
                    'height' => Image::make($photo)->height(),
                    'width' => Image::make($photo)->width()
                ]);
    
            }
        }

        return response()->json('success');

    }

    public function addLike(int $eventId){

        $event = Evenement::findOrFail($eventId);

        $event->likes()->create([
            'user_id' => Auth::user()->id,
        ]);

        return Response()->json(['message' => "SUCCESSFULL"]);
    }


    public function show(Evenement $evenement)
    {
        //
    }

    public function edit(Evenement $evenement)
    {
        //
    }

    public function update(Request $request, Evenement $evenement)
    {
        //
    }


    public function destroy(Evenement $evenement)
    {
        //
    }
}
