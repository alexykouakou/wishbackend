<?php

namespace App\Http\Controllers;

use App\Models\Profil;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function create()
    {
        //
    }




    public function show(Profil $profil)
    {
        //
    }


    public function edit(Profil $profil)
    {
        //
    }


    public function store(Request $request)
    {
        $user = Auth::user();

        $input =  $request->validate([

            'choixcouleur_id' => 'required',
            'paysreve_id' => 'required',
            'user_id' => 'required',
            'capitale_pays' => 'required',
            'population_pays' => 'required',
         ]);

        $user = Profil::create([
            'choixcouleur_id' => $request->choixcouleur_id,
            'paysreve_id' => $request->paysreve_id,
            'capitale_pays' => $request->capitale_pays,
            'population_pays' => $request->population_pays,
            // 'user_id' => $request->user_id = $Userprofil
            // 'birthday' => Carbon::parse($request->birthday)
        ]);

        $imagePath = $request->file('photo')->store('photoProfils', 'public');
        if (!empty($request->photo)) {
            $input['photo'] = $imagePath;
            $user = Profil::create([
                "choixcouleur_id" => $request->choixcouleur_id,
                "paysreve_id" => $request->paysreve_id,
                "photo" => $imagePath,
                "capitale_pays" => $request->capitale_pays,
                "population_pays" => $request->population_pays,
                "user" => $user,
            ]);
        }

        return response()->json($user, '200');
    }



    public function destroy(Profil $profil)
    {
        //
    }
}
