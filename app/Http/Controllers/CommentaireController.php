<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Commentaire;
use App\Models\Evenement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function postcommentaireUser(Request $request)
    {
        
        $evenement  = Evenement::find($request->evenement_id);
        if(!$evenement) {return response()->json('not_found', 404);}
        
        $user  = auth()->user();

        $commentaire = Commentaire::create([
            "evenement_id" => $evenement->id,
            "user_id" => $user->id,
            "message" => $request->message,
            'nb_of_listens' => 0
        ]);
        
        if($request->file('audio')) {
            $audioPath = $request->file('audio')->store('commentAudio', 'public');
            $commentaire->audio = $audioPath;
            $commentaire->save();
        }
        
        
        $response = [
            'total_commentaires' => $evenement->commentaires()->count(),
            'commentaire' => $evenement->commentaires()->where('id', $commentaire->id)->first(), //pour recuperer l'user dans le resultat
            "commentaires" => $evenement->commentaires()->latest()->get(),
        ];
        
        return Response()->json($response);
      
    }

    public function addLike(int $commentId){
        $comment = Commentaire::findOrFail($commentId);

        $comment->likes()->create([
            'user_id' => Auth::user()->id,
        ]);

        return Response()->json(['message' => "SUCCESSFULL"]);
    }

    public function incrementNbOfListens(int $commentId){
        $comment = Commentaire::findOrFail($commentId);

        $comment->nb_of_listens += 1;

        $comment->save();

        return Response()->json(['message' => "SUCCESSFULL"]);
    }


    public function show(Commentaire $commentaire)
    {
        //
    }


    public function edit(Commentaire $commentaire)
    {
        //
    }


    public function update(Request $request, Commentaire $commentaire)
    {
        //
    }


    public function destroy(Commentaire $commentaire)
    {
        //
    }
}
