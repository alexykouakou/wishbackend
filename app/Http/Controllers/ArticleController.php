<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::all();

        return response()->json(["success" => $articles]);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $input =  $request->validate([

            "nom_produit" => "required",
            "prix_produit" => "required",
            "description" => "required",
            "quantite" => "integer|min:1",
            "image_article" => "required"
        ]);

        $imagePath = $request->file('image_article')->store('photoArticles', 'public');
        if (!empty($request->image_article)) {
            $input['image_article'] = $imagePath;
            $input = Article::create($input);

            return Response()->json(["success" => $input]);
        }
    }


    public function show(User $user)
    {
    }


    public function edit(Article $article)
    {
        //
    }


    public function update(Request $request, Article $article)
    {
        $request->validate([
            "nom_produit" => "required",
            "prix_produit" => "required",
            "description" => "required",
            "quantite" => "required",
            "image_article" => "required"
        ]);

        $article->update($request->all());

        return response()->json(["success" => $article]);
        // return redirect()->route('titreprospect.index')->with('success', 'Mise éffectué avec succès');
    }


    public function destroy(Article $article)
    {
        $article->delete();
        return response()->json(["success" => $article]);
    }
}
