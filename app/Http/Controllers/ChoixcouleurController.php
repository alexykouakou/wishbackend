<?php

namespace App\Http\Controllers;

use App\Models\Choixcouleur;
use Illuminate\Http\Request;

class ChoixcouleurController extends Controller
{

    /**
     * @OA\Post(
     *      path="/api/ajoutercouleur",
     *      operationId="ajoutercouleur",
     *      tags={"Ajouter une couleur"},
     *      summary="Permet d'ajoute les couleurs",
     *      description="Retourner les couleurs ",
     *@OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"couleur_favories"},
     *       @OA\Property(property="couleur_favories", type="string", example="Vert"),
     *     
     *     
     *    ),
     * ),
   
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="erreur  veillez  ressaisir",
     *      ),
     *      
     *     )
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

  

    public function ajoutercouleur(Request $request)
    {

        $request->validate([

            "couleur_favories" => "required"
        ]);

        $choixcouleur = Choixcouleur::create($request->all());

        return response()->json(["success" => $choixcouleur]);
    }

  
    public function show(Choixcouleur $choixcouleur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Choixcouleur  $choixcouleur
     * @return \Illuminate\Http\Response
     */
    public function edit(Choixcouleur $choixcouleur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Choixcouleur  $choixcouleur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Choixcouleur $choixcouleur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Choixcouleur  $choixcouleur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Choixcouleur $choixcouleur)
    {
        //
    }
}
