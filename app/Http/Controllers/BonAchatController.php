<?php

namespace App\Http\Controllers;

use App\Models\BonAchat;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreBonAchatRequest;
use App\Http\Requests\UpdateBonAchatRequest;

class BonAchatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBonAchatRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBonAchatRequest $request)
    {
        $bonAchat = $request->validate([
            "montant" => "required",
            "Nbre_bonAchat" => "required",
            "type_achat_id" => "required",
        ]);






        

        $bonAchat = new BonAchat();
        $bonAchat = Auth::user()->id;
        $bonAchat->type_achat_id = request()->type_achat_id;
        $bonAchat->montant = request()->montant;
        $bonAchat->Nbre_bonAchat = request()->Nbre_bonAchat;
        $bonAchat->save();

        return response()->json(['bonAchat' => $bonAchat]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BonAchat  $bonAchat
     * @return \Illuminate\Http\Response
     */
    public function show(BonAchat $bonAchat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BonAchat  $bonAchat
     * @return \Illuminate\Http\Response
     */
    public function edit(BonAchat $bonAchat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBonAchatRequest  $request
     * @param  \App\Models\BonAchat  $bonAchat
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBonAchatRequest $request, BonAchat $bonAchat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BonAchat  $bonAchat
     * @return \Illuminate\Http\Response
     */
    public function destroy(BonAchat $bonAchat)
    {
        //
    }
}
