<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Chequecadeau;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreChequecadeauRequest;
use App\Http\Requests\UpdateChequecadeauRequest;

class ChequecadeauController extends Controller
{


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreChequecadeauRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChequecadeauRequest $request)
    {
        $chequecadeau = $request->validate([
            "montant" => "required",
            "commentaire" => "required",
            "code_bar" => "required"

        ]);

        $user = Auth::user()->id;
        // $user = new User();
        $initialNom = $request->nom;
        $initialPrenom = $request->prenom;
        $initialPhone = $request->phone;
        $initial1 = $initialNom[0];
        $initial2 = $initialPrenom[0];
        $initial3 =   $initialPhone[0];
        // $initial3 = $initial1$initial2;
        $codebar = $initial1 . $initial2 . $initial3 . rand(1, 9999) . '-' . "WISHMEE";

        if ($codebar != null) {
            $input['code_bar'] =  $codebar;
            $chequecadeau = $request->update([
                "code_bar" => $codebar,
                "montant" => $request->montant,
                "commentaire" => $request->commentaire,
                // "user_id"=>$user,

            ]);

            return response()->json($chequecadeau, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Chequecadeau  $chequecadeau
     * @return \Illuminate\Http\Response
     */
    public function show(Chequecadeau $chequecadeau)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chequecadeau  $chequecadeau
     * @return \Illuminate\Http\Response
     */
    public function edit(Chequecadeau $chequecadeau)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateChequecadeauRequest  $request
     * @param  \App\Models\Chequecadeau  $chequecadeau
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChequecadeauRequest $request, Chequecadeau $chequecadeau)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chequecadeau  $chequecadeau
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chequecadeau $chequecadeau)
    {
        //
    }
}
