<?php

namespace App\Http\Controllers;

use App\Models\Evenement;
use App\Models\Hashtag;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function getHastags($hashtag)
    {
        $hashtags = Hashtag::where('libele', 'like', $hashtag)->get();

        return response()->json(['hashtags' => $hashtags]);
    }
}
