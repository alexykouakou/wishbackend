<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Models\CommandArticle;
use Illuminate\Support\Facades\Auth;
use Facade\FlareClient\Http\Response;

class CommandArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $commande_article = $request->validate([
            "user_id"=>"required",
            "article_id"=>"required",
        ]);
        $commande_article = Article::find($commande_article['article_id']);
        if($commande_article){
         $commandArticles = CommandArticle::create([
             "user_id"=>  $commande_article,
             "article_id"=>$request->article_id,
         ]) ;  

        }
        return Response()->json(["success"=>$commandArticles]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommandArticle  $commandArticle
     * @return \Illuminate\Http\Response
     */
    public function show(CommandArticle $commandArticle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommandArticle  $commandArticle
     * @return \Illuminate\Http\Response
     */
    public function edit(CommandArticle $commandArticle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommandArticle  $commandArticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommandArticle $commandArticle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommandArticle  $commandArticle
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommandArticle $commandArticle)
    {
        //
    }
}
