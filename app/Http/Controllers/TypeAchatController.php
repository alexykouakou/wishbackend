<?php

namespace App\Http\Controllers;


use App\Models\TypeAchat;
use Database\Seeders\TypeAchatSeeder;
use Illuminate\Http\Request;

class TypeAchatController extends Controller
{

    public function getbonAchat()
    {
        $achatbon = TypeAchat::get();

        return  Response()->json(["success" => $achatbon]);
    }


    public function create()
    {
        //
    }

    public function postbonAchat(Request $request)
    {

        $request->validate([

            "name" => "required",
            "montant" => "required"
        ]);

        $typeachat = TypeAchat::create($request->all());
        return Response()->json(['success' => $typeachat]);
    }


    public function show(TypeAchat $typeAchat)
    {
        //
    }


    public function edit(TypeAchat $typeAchat)
    {
        //
    }


    public function update(Request $request, TypeAchat $typeAchat)
    {
        //
    }


    public function destroy(TypeAchat $typeAchat)
    {
        //
    }
}
