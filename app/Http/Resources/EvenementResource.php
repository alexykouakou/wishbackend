<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use Config;

class EvenementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        Carbon::setLocale('fr');
        $default_image = ['id'=> 0, 'path'=> Config::get('app.url').'/storage/evenements/no-photo.jpg', 'width'=> '572', 'height'=> '700'];


        return [
            'id' => $this->id,
            'titre' => $this->titre,
            'description' => $this->description,
            'type' => $this->type ? 'post' : 'event',
            'photo_evenement' => $this->images()->first() ? new ImageResource($this->images()->first()) : $default_image,
            'images' => ImageResource::collection($this->images()->get()),
            'localisation' => $this->localisation,
            'user' => $this->user,
            'created_at' => Carbon::parse($this->created_at)->diffForHumans(),
            'commentaire' => $this->commentaires()->latest()->first(),
            'total_commentaires' => $this->commentaires()->count(),
        ];
    }
}
