<?php

use App\Models\Choixcouleur;
use Illuminate\Http\Request;
use Database\Seeders\PaysreveSeeder;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PanierController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\BonAchatController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\PaysreveController;
use App\Http\Controllers\EvenementController;
use App\Http\Controllers\TypeAchatController;
use App\Http\Controllers\CommentaireController;
use App\Http\Controllers\ChequecadeauController;
use App\Http\Controllers\ChoixcouleurController;
use App\Http\Controllers\AuthentificateController;
use App\Http\Controllers\CommandArticleController;

Route::post('/postlogin', [UserController::class, 'postlogin'])->name('layouts.postlogin');

// Api de inscription
Route::post('/postinscription', [UserController::class, 'postinscription'])->name('layouts.postinscription');
Route::post('/postinscription/phone', [UserController::class, 'postinscriptionphone']);
Route::post('/postinscription/phone/validate', [UserController::class, 'postinscriptionphonevalidate']);

Route::post('/reset-password', [UserController::class, 'resetPassword'])->name('reset-password');
// route::get('/nouveau-mot-de-passe/{id_user}', [UserController::class, 'new_password'])->whereNumber('id_user')->name('set-credential');
Route::post('/new-password', [UserController::class, 'set_new_password'])->name('set-new-password');

Route::post('/new-password/phone', [UserController::class, 'set_new_password_phone']);

Route::group(['middleware' => ['auth:sanctum']], function () {

    // Deconnexion
    Route::get('/logout', [UserController::class, 'logout'])->name('logout');
    // Api profile utilisateur
    Route::post('/createProfile', [UserController::class, 'profileuser'])->name('layouts.profileuser');
    //preferance de couleur et  profile
    Route::post('ajoutercouleur', [ChoixcouleurController::class, 'ajoutercouleur'])->name('layouts.choixcouleur');
    //Api type bon achat
    Route::post('/postbonAchat', [TypeAchatController::class, 'postbonAchat'])->name('layouts.postbonAchat');
    //Api pour afficher les bon achats
    Route::get('/getbonAchat', [TypeAchatController::class, 'getbonAchat'])->name('layouts.getbonAchat');
    // Api pour poster un evenement
    Route::post('/postevent', [EvenementController::class, 'postevent'])->name('layouts.postevent');
    Route::post('/postevent/event', [EvenementController::class, 'posteventevent']);
    // Api pour liste des evenements poste par un utilisateur
    Route::get('/getevents', [EvenementController::class, 'getevents'])->name('layouts.getevents');
    // Api pour poster un type evenement
    Route::post('/posteventType', [EvenementController::class, 'posteventType'])->name('layouts.posteventType');
    // Api de profile utilisateur
    Route::put('/showprofile/{user}', [UserController::class, 'showprofile'])->name('layouts.showprofile');

    // Ajouter un commentaire
    Route::post('/postcommentaireUser', [CommentaireController::class, 'postcommentaireUser'])->name('layouts.postcommentaireUser');
    Route::post('ajouterpaysReserve', [PaysreveController::class, 'ajouterpaysReserve'])->name('layouts.ajouterpaysReserve');
    Route::get('getpaysReserve', [PaysreveController::class, 'index'])->name('layouts.getpaysReserve');

    Route::resource('storearticle', ArticleController::class);
    //poster une commande articles
    Route::resource('CommandArticle', CommandArticleController::class);
    // la route des informations propre aux commandes
    //Route::resource('commande', CommandeController::class);
    // la route des paniers
    Route::resource('chequecadeau',  ChequecadeauController::class);
    Route::resource('panier',   PanierController::class);
    Route::resource('user',     UserController::class);
    Route::resource('profile',  ProfilController::class);
    Route::resource('commande',  CommandeController::class);
    Route::resource('bonachat',  BonAchatController::class);
    Route::get('/get-events', [EvenementController::class, 'getRandomEvents']);
    Route::post('/search-events', [EvenementController::class, 'searchEvent']);

    Route::get('/get-users-suggestion', [UserController::class, 'getSuggestion']);
    Route::get('/users-to-mention', [UserController::class, 'getUsersToMention']);
    Route::post('/subscribe-to-user', [UserController::class, 'subscribe']);
    Route::post('/unsubscribe-to-user', [UserController::class, 'unSubscribe']);

    Route::get('/get-hashtags/{hashtag}', [SearchController::class, 'getHastags']);

    Route::post('/send-message', [MessageController::class, 'sendMessage']);
    Route::post('/add-like-event/{eventId}', [EvenementController::class, 'addLike']);
    Route::post('/add-like-comment/{commentId}', [CommentaireController::class, 'addLike']);
    Route::post('/increment-listen/{commentId}', [CommentaireController::class, 'incrementNbOfListens']);
});
